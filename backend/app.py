from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from prometheus_flask_exporter import PrometheusMetrics
import pymysql

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://cruduser:secretpasswd00@mariadb:3306/myapp"
app.json.sort_keys = False
db = SQLAlchemy(app)
metrics = PrometheusMetrics(app, group_by='endpoint')

class Customers(db.Model):
    __tablename__ = 'Customers'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    second_name = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), nullable=False)

    def serialize(self):
        data = {
            'id': self.id,
            'name': self.name,
            'second_name': self.second_name,
            'email': self.email
        }
        order_data = ['id', 'name', 'second_name', 'email']
        return {
            key: data[key] for key in order_data
        }

    def __repr__(self):
        return f"Customer('{self.name}', '{self.second_name}', '{self.email}'"

@app.route('/api/customers', methods=['POST'])
def create_customer():
    data = request.json
    new_customer = Customers(name=data['name'], second_name=data['second_name'], email=data['email'])
    db.session.add(new_customer)
    db.session.commit()
    return jsonify({'customer': new_customer.serialize(), 'message':'Customer created successfully!'}), 201

@app.route('/api/customers/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def customer_action(id):
    customer = Customers.query.get_or_404(id)
    if request.method == 'GET':
        return jsonify({'customer': customer.serialize()})
    elif request.method == 'PUT':
        data = request.json
        customer.name = data.get('name', customer.name)
        customer.second_name = data.get('second_name', customer.second_name)
        customer.email = data.get('email', customer.email)
        db.session.commit()
        return jsonify({'customer_id': customer.id, 'message':'Customer updated!'}), 200
    elif request.method == 'DELETE':
        db.session.delete(customer)
        db.session.commit()
        return jsonify({'customer_id': customer.id, 'message': 'Customer deleted!'}), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5000)