GRANT ALL PRIVILEGES ON *.* TO 'cruduser'@'%';
FLUSH PRIVILEGES;
USE myapp;
CREATE TABLE Customers (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    second_name VARCHAR(50),
    email VARCHAR(25)
);
